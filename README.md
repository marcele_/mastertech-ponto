# API para controle de ponto
* Mastertech | Trilha 3
* Atividade Diagnóstica: API para controle de ponto
* Sistema simples para controle de ponto de uma empresa.

## Endpoints
**Usuários**

* POST /usuarios\
Inclui um novo usuário na base.\
`{
    "nomeCompleto": "Nome Sobrenome",
    "cpf": "02798805023",
    "email": "email@email.com.br"
}`

* GET /usuarios\
Lista com todos os usuários cadastrados na base.
* GET /usuarios/{id}\
Exibição de dados do usuário de acordo com um id informado.
* PUT /usuarios/{id}\
alteração dos dados do usuário identificado pelo id.\
`{
    "nomeCompleto": "Nome Sobrenome",
    "cpf": "02798805023",
    "email": "email@email.com.br"
}`
* DELETE /usuarios/{id}\
Apaga o registro do usuário identificado pelo id.

**Registro de Ponto**

* /pontos/registrar\
Cadastra uma batida de ponto (seja ENTRADA ou SAIDA) para um usuário específico, de
acordo com o id (usuário) informado.\
`{
    "tipoBatida": "SAIDA",   
    "usuario": {
        "id": 8
    }
}`

* /pontos\
listagem de todas as batidas de ponto

* /pontos/usuario/{id}\
Listagem com todas as batidas de ponto de um único usuário, de acordo com o id
informado.
