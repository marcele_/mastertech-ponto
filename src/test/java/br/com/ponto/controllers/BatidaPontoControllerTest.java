package br.com.ponto.controllers;

import br.com.ponto.enums.TipoBatidaEnum;
import br.com.ponto.models.BatidaPonto;
import br.com.ponto.models.Usuario;
import br.com.ponto.models.dtos.BatidaPontoDTO;
import br.com.ponto.models.dtos.EspelhoPontoRespostaDTO;
import br.com.ponto.services.BatidaPontoService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@WebMvcTest(BatidaPontoController.class)
public class BatidaPontoControllerTest {

    @Autowired
    private BatidaPontoController batidaPontoController;

    @MockBean
    private BatidaPontoService batidaPontoService;

    @Autowired
    private MockMvc mockMvc;

    private Usuario usuario;
    private BatidaPonto batidaPonto;
    private BatidaPontoDTO batidaPontoDTO;
    private EspelhoPontoRespostaDTO espelhoPontoRespostaDTO;

    @BeforeEach
    public void setUp() {
        usuario = new Usuario();
        usuario.setId(1);
        usuario.setNomeCompleto("Nome Completo do Usuário de Teste");
        usuario.setCpf("16150466078");
        usuario.setEmail("usuario@email.com.br");
        usuario.setDataCadastro(LocalDate.now());

        batidaPonto = new BatidaPonto();
        batidaPonto.setId(1);
        batidaPonto.setTipoBatida(TipoBatidaEnum.ENTRADA);
        batidaPonto.setDataHoraBatida(LocalDateTime.now());
        batidaPonto.setUsuario(usuario);

        batidaPonto = new BatidaPonto();
        batidaPonto.setId(2);
        batidaPonto.setTipoBatida(TipoBatidaEnum.SAIDA);
        batidaPonto.setDataHoraBatida(LocalDateTime.now());
        batidaPonto.setUsuario(usuario);

        batidaPontoDTO = new BatidaPontoDTO();
        batidaPontoDTO.setIdBatidaPonto(1);
        batidaPontoDTO.setTipoBatida(TipoBatidaEnum.ENTRADA);
        batidaPontoDTO.setDataHoraBatida(LocalDateTime.now());

        batidaPontoDTO = new BatidaPontoDTO();
        batidaPontoDTO.setIdBatidaPonto(2);
        batidaPontoDTO.setTipoBatida(TipoBatidaEnum.SAIDA);
        batidaPontoDTO.setDataHoraBatida(LocalDateTime.now());

        espelhoPontoRespostaDTO = new EspelhoPontoRespostaDTO();
        //espelhoPontoRespostaDTO.setBatidaPontoDTOS(batidaPontoDTO);
        espelhoPontoRespostaDTO.setTotalHorasTrabalhadas(LocalTime.now().toString());
    }

    @Test
    public void testarCadastrarBatidaPontoComErro() throws Exception{
        Mockito.when(batidaPontoService.salvar(Mockito.any(BatidaPonto.class))).then(retorno->{
            throw new RuntimeException("Erro no registro de ponto!");
        });

        mockMvc.perform(MockMvcRequestBuilders.post("/pontos/registrar"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

   /* @Test
    public void testarCadastrarBatidaPonto() throws Exception{
        Mockito.when(batidaPontoService.salvar(Mockito.any(BatidaPonto.class))).then(resultado ->{
            return batidaPonto;
        });

        mockMvc.perform(MockMvcRequestBuilders.post("/pontos/registrar"))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

      @Test
    public void testarCadastrarBatidaPonto() throws Exception{
        Mockito.when(batidaPontoService.salvar(Mockito.any(BatidaPonto.class))).then(usuarioObjeto->{
            return espelhoPontoRespostaDTO;
        });

        ObjectMapper mapper = new ObjectMapper();
        String jsonBatidaPonto = mapper.writeValueAsString(espelhoPontoRespostaDTO);

        mockMvc.perform(MockMvcRequestBuilders.post("/pontos/registrar")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonBatidaPonto))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }*/

   /* @Test
    public void testarListarTodasBatidasPontos() throws Exception{
        Mockito.when(batidaPontoService.consultar()).then(usuarioObjeto->{
            List<EspelhoPontoRespostaDTO> batidaPontoRespostaDTOS = new ArrayList<>();
            batidaPontoRespostaDTOS.add(espelhoPontoRespostaDTO);
            batidaPontoRespostaDTOS.add(espelhoPontoRespostaDTO);
            batidaPontoRespostaDTOS.add(espelhoPontoRespostaDTO);
            return espelhoPontoRespostaDTO;
        });
        mockMvc.perform(MockMvcRequestBuilders.get("/pontos"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(3));
    }

    @Test
    public void testarConsultarBatidaPontoPorIdExistente() throws Exception{
        Mockito.when(batidaPontoService.buscarPorIdUsuario(Mockito.anyInt())).then(usuarioObjeto->{
            return espelhoPontoRespostaDTO;
        });

        ObjectMapper mapper = new ObjectMapper();
        String jsonUsuario = mapper.writeValueAsString(espelhoPontoRespostaDTO);

        mockMvc.perform(MockMvcRequestBuilders.get("/pontos/usuarios/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonUsuario))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));
    }*/

    @Test
    public void testarConsultarBatidaPontoPorIdInexistente() throws Exception{
        Mockito.when(batidaPontoService.buscarPorIdUsuario(Mockito.anyInt())).then(retorno->{
            throw new RuntimeException("Erro. Usuário sem registro de ponto.");
        });
        mockMvc.perform(MockMvcRequestBuilders.get("/pontos/usuario/1"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

}
