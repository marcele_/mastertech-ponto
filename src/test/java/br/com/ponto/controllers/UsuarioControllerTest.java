package br.com.ponto.controllers;

import br.com.ponto.models.Usuario;
import br.com.ponto.models.dtos.UsuarioRespostaDTO;
import br.com.ponto.services.UsuarioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@WebMvcTest(UsuarioController.class)
public class UsuarioControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UsuarioController usuarioController;

    @MockBean
    private UsuarioService usuarioService;

    private Usuario usuario;

    private UsuarioRespostaDTO usuarioRespostaDTO;

    @BeforeEach
    public void setUp() {
        usuario = new Usuario();
        usuario.setId(1);
        usuario.setNomeCompleto("Nome Completo do Usuário de Teste");
        usuario.setCpf("16150466078");
        usuario.setEmail("usuario@email.com.br");
        //usuario.setDataCadastro(LocalDate.now());

        usuarioRespostaDTO = new UsuarioRespostaDTO();
        usuarioRespostaDTO.setId(1);
        usuarioRespostaDTO.setNomeCompleto("Nome Completo do Usuário de Teste");
        usuarioRespostaDTO.setCpf("16150466078");
        usuarioRespostaDTO.setEmail("usuario@email.com.br");
        //usuarioRespostaDTO.setDataCadastro(LocalDate.now());
    }

    @Test
    public void testarCriarUsuario() throws Exception{
        Mockito.when(usuarioService.salvar(Mockito.any(Usuario.class))).then(usuarioObjeto->{
            usuario.setDataCadastro(LocalDate.now());
            usuarioRespostaDTO.setDataCadastro(LocalDate.now());
            return usuarioRespostaDTO;
        });

        ObjectMapper mapper = new ObjectMapper();
        String jsonUsuario = mapper.writeValueAsString(usuarioRespostaDTO);

        mockMvc.perform(MockMvcRequestBuilders.post("/usuarios")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonUsuario))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.dataCadastro", CoreMatchers.equalTo(LocalDate.now().toString())));
    }

    @Test
    public void testarAlterarUsarioExistente() throws Exception{
        Mockito.when(usuarioService.editar(Mockito.anyInt(),Mockito.any(Usuario.class))).then(usuarioObjeto->{
            usuario.setDataCadastro(LocalDate.now());
            usuarioRespostaDTO.setDataCadastro(LocalDate.now());
            return usuarioRespostaDTO;
        });

        ObjectMapper mapper = new ObjectMapper();
        String jsonUsuario = mapper.writeValueAsString(usuarioRespostaDTO);

        mockMvc.perform(MockMvcRequestBuilders.put("/usuarios/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonUsuario))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));
    }

    @Test
    public void testarAlterarUsarioInexistente() throws Exception{
        Mockito.when(usuarioService.editar(Mockito.anyInt(),Mockito.any(Usuario.class))).then(usuarioObjeto->{
            throw new RuntimeException("Usuario inexistente!");
        });

        ObjectMapper mapper = new ObjectMapper();
        String jsonDeUsuario = mapper.writeValueAsString(usuario);

        mockMvc.perform(MockMvcRequestBuilders.put("/usuarios/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeUsuario))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarConsultarUsuarioPorIdExistente() throws Exception{
        Mockito.when(usuarioService.buscarPorId(Mockito.anyInt())).then(usuarioObjeto->{
            usuario.setDataCadastro(LocalDate.now());
            usuarioRespostaDTO.setDataCadastro(LocalDate.now());
            return usuarioRespostaDTO;
        });

        ObjectMapper mapper = new ObjectMapper();
        String jsonUsuario = mapper.writeValueAsString(usuarioRespostaDTO);

        mockMvc.perform(MockMvcRequestBuilders.get("/usuarios/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonUsuario))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));
    }

    @Test
    public void testarConsultarUsuarioPorIdInexistente() throws Exception{
        Mockito.when(usuarioService.buscarPorId(Mockito.anyInt())).then(usuarioObjeto->{
            throw new RuntimeException("Usuario Inexistente!");
        });

        ObjectMapper mapper = new ObjectMapper();
        String jsonUsuario = mapper.writeValueAsString(usuario);

        mockMvc.perform(MockMvcRequestBuilders.get("/usuarios/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonUsuario))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarListarTresUsuariosExistentes() throws Exception{
        Mockito.when(usuarioService.buscar()).then(usuarioObjeto->{
            List<Usuario> usuarios = new ArrayList<>();
            usuarios.add(usuario);
            usuarios.add(usuario);
            usuarios.add(usuario);
            return usuarios;
        });

        mockMvc.perform(MockMvcRequestBuilders.get("/usuarios"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(3));
    }

    @Test
    public void testarListarTodosUsuariosInexistentes() throws Exception{
        Mockito.when(usuarioService.buscar()).then(usuarioObjeto->{
            throw new RuntimeException();
        });

        mockMvc.perform(MockMvcRequestBuilders.get("/usuarios"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

}
