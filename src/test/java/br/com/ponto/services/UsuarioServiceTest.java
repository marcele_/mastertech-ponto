package br.com.ponto.services;

import br.com.ponto.models.Usuario;
import br.com.ponto.models.dtos.UsuarioRespostaDTO;
import br.com.ponto.repositories.UsuarioRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class UsuarioServiceTest {

    @Autowired
    private UsuarioService usuarioService;

    @MockBean
    private UsuarioRepository usuarioRepository;

    private Usuario usuario;

    private UsuarioRespostaDTO usuarioRespostaDTO;

    @BeforeEach
    public void setUp() {
        usuario = new Usuario();
        usuario.setNomeCompleto("Nome Completo do Usuário de Teste");
        usuario.setCpf("16150466078");
        usuario.setEmail("usuario@email.com.br");
        usuario.setDataCadastro(LocalDate.now());
    }

    @Test
    public void testarSalvar() {
        Mockito.when(usuarioRepository.save(Mockito.any(Usuario.class))).thenReturn(usuario);
        UsuarioRespostaDTO usuarioRespostaDTO = usuarioService.salvar(usuario);
        usuarioRespostaDTO.converterUsuarioRespostaDTO(usuario);

        Assertions.assertEquals(usuarioRespostaDTO.getId(), usuario.getId());
        Assertions.assertEquals(usuarioRespostaDTO.getCpf(), usuario.getCpf());
        Assertions.assertEquals(usuarioRespostaDTO.getEmail(), usuario.getEmail());
        Assertions.assertEquals(usuarioRespostaDTO.getNomeCompleto(), usuario.getNomeCompleto());
        Assertions.assertEquals(usuarioRespostaDTO.getDataCadastro(), usuario.getDataCadastro());
    }

    @Test
    public void testarBuscarTodosUsuarios() {
        Mockito.when(usuarioRepository.findAll()).thenReturn(Arrays.asList(usuario));
        List<UsuarioRespostaDTO> usuarioRespostaDTOS = usuarioService.buscar();

        Assertions.assertEquals(usuarioRespostaDTOS.size(),Arrays.asList(usuario).size());
    }

    @Test
    public void testarBuscarPorIdExistente() {
        Mockito.when(usuarioRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(usuario));

        UsuarioRespostaDTO usuarioRespostaDTO = usuarioService.buscarPorId(1);
        Assertions.assertEquals(usuarioRespostaDTO.getId(), usuario.getId());
        Assertions.assertEquals(usuarioRespostaDTO.getCpf(), usuario.getCpf());
        Assertions.assertEquals(usuarioRespostaDTO.getEmail(), usuario.getEmail());
        Assertions.assertEquals(usuarioRespostaDTO.getNomeCompleto(), usuario.getNomeCompleto());
    }

    @Test
    public void testarBuscarPorIdInexistente() {
        Mockito.when(usuarioRepository.findById(Mockito.anyInt())).thenReturn(Optional.empty());

        Assertions.assertThrows(RuntimeException.class,
                () -> {
                    usuarioService.buscarPorId(1);
                });
    }

    @Test
    public void testarEditarUsuarioExistente(){
        Mockito.when(usuarioRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(usuario));
        Mockito.when(usuarioRepository.save(Mockito.any(Usuario.class))).thenReturn(usuario);

        UsuarioRespostaDTO usuarioRespostaDTO = usuarioService.editar(1,usuario);

        Assertions.assertEquals(usuarioRespostaDTO.getId(), usuario.getId());
        Assertions.assertEquals(usuarioRespostaDTO.getCpf(), usuario.getCpf());
        Assertions.assertEquals(usuarioRespostaDTO.getEmail(), usuario.getEmail());
        Assertions.assertEquals(usuarioRespostaDTO.getNomeCompleto(), usuario.getNomeCompleto());
        Assertions.assertEquals(usuarioRespostaDTO.getDataCadastro(), usuario.getDataCadastro());
    }

    @Test
    public void testarEditarUsuarioInexistente(){
        Mockito.when(usuarioRepository.findById(Mockito.anyInt())).thenReturn(Optional.empty());
        Assertions.assertThrows(RuntimeException.class,
                () -> {
                    usuarioService.editar(1,usuario);
                });
    }

}
