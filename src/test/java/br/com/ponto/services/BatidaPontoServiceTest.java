package br.com.ponto.services;

import br.com.ponto.enums.TipoBatidaEnum;
import br.com.ponto.models.BatidaPonto;
import br.com.ponto.models.Usuario;
import br.com.ponto.models.dtos.BatidaPontoDTO;
import br.com.ponto.models.dtos.EspelhoPontoRespostaDTO;
import br.com.ponto.repositories.BatidaPontoRepository;
import br.com.ponto.repositories.UsuarioRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Optional;

@SpringBootTest
public class BatidaPontoServiceTest {

    @Autowired
    private BatidaPontoService batidaPontoService;

    @MockBean
    private BatidaPontoRepository batidaPontoRepository;

    @MockBean
    private UsuarioRepository usuarioRepository;

    private BatidaPonto batidaPonto;

    private BatidaPontoDTO batidaPontoDTO;

    private EspelhoPontoRespostaDTO espelhoPontoRespostaDTO;

    private Usuario usuario;

    @BeforeEach
    public void setUp() {
        usuario = new Usuario();
        usuario.setId(1);
        usuario.setNomeCompleto("Nome Completo do Usuário de Teste");
        usuario.setCpf("16150466078");
        usuario.setEmail("usuario@email.com.br");
        usuario.setDataCadastro(LocalDate.now());

        batidaPonto = new BatidaPonto();
        batidaPonto.setId(1);
        batidaPonto.setTipoBatida(TipoBatidaEnum.ENTRADA);
        batidaPonto.setDataHoraBatida(LocalDateTime.now());
        batidaPonto.setUsuario(usuario);

        batidaPonto = new BatidaPonto();
        batidaPonto.setId(2);
        batidaPonto.setTipoBatida(TipoBatidaEnum.SAIDA);
        batidaPonto.setDataHoraBatida(LocalDateTime.now());
        batidaPonto.setUsuario(usuario);

        batidaPontoDTO = new BatidaPontoDTO();
        batidaPontoDTO.setIdBatidaPonto(1);
        batidaPontoDTO.setTipoBatida(TipoBatidaEnum.ENTRADA);
        batidaPontoDTO.setDataHoraBatida(LocalDateTime.now());

        batidaPontoDTO = new BatidaPontoDTO();
        batidaPontoDTO.setIdBatidaPonto(2);
        batidaPontoDTO.setTipoBatida(TipoBatidaEnum.SAIDA);
        batidaPontoDTO.setDataHoraBatida(LocalDateTime.now());
    }

    @Test
    public void testarSalvarRegistroPontoUsuarioExistente(){
        Mockito.when(usuarioRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(usuario));
        Mockito.when(batidaPontoRepository.buscarUltimoResgistroPontoDia(Mockito.anyInt())).thenReturn("SAIDA");
        Mockito.when(batidaPontoRepository.save(Mockito.any(BatidaPonto.class))).then(resultado ->{
            return batidaPonto;
        });

        BatidaPonto batidaPontoResultado =
                batidaPontoService.salvar(batidaPonto);

        batidaPontoDTO.converterBatidaPontoDTO(batidaPonto);

        Assertions.assertEquals(batidaPontoDTO.getIdBatidaPonto(), batidaPontoResultado.getId());
        Assertions.assertEquals(batidaPontoDTO.getTipoBatida(), batidaPontoResultado.getTipoBatida());
        Assertions.assertEquals(batidaPontoDTO.getDataHoraBatida(), batidaPontoResultado.getDataHoraBatida());
    }

    @Test
    public void testarSalvarRegistroPontoUsuarioInexistente(){
        Mockito.when(usuarioRepository.findById(Mockito.anyInt())).thenReturn(Optional.empty());

        Assertions.assertThrows(RuntimeException.class,
                () -> {
                    batidaPontoService.salvar(batidaPonto);
                });
    }
}
