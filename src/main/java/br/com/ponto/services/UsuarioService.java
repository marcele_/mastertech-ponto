package br.com.ponto.services;

import br.com.ponto.models.Usuario;
import br.com.ponto.models.dtos.UsuarioRespostaDTO;
import br.com.ponto.repositories.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    public UsuarioRespostaDTO salvar(Usuario usuario){
        usuario.setDataCadastro(LocalDate.now());
        Usuario usuarioObjeto = usuarioRepository.save(usuario);

        UsuarioRespostaDTO usuarioRespostaDTO = new UsuarioRespostaDTO();
        usuarioRespostaDTO.converterUsuarioRespostaDTO(usuarioObjeto);
        return usuarioRespostaDTO;
    }

    public List<UsuarioRespostaDTO> buscar(){
        Iterable<Usuario> listaUsuarios = usuarioRepository.findAll();
        List<UsuarioRespostaDTO> listausuarioRespostaDTO = new ArrayList<>();

        UsuarioRespostaDTO usuarioRespostaDTO;
        for (Usuario usuario: listaUsuarios) {
            usuarioRespostaDTO = new UsuarioRespostaDTO();
            usuarioRespostaDTO.converterUsuarioRespostaDTO(usuario);
            listausuarioRespostaDTO.add(usuarioRespostaDTO);
        }
        return listausuarioRespostaDTO;
    }

    public UsuarioRespostaDTO buscarPorId(int id){
        Optional<Usuario> usuarioOptional = usuarioRepository.findById(id);
        if (usuarioOptional.isPresent()){
            UsuarioRespostaDTO usuarioRespostaDTO = new UsuarioRespostaDTO();
            usuarioRespostaDTO.converterUsuarioRespostaDTO(usuarioOptional.get());
            return usuarioRespostaDTO;
        } else {
            throw new RuntimeException("O usuário " + id + " não foi encontrado.");
        }
    }

    public UsuarioRespostaDTO editar(int id, Usuario usuario) {
        Optional<Usuario> usuarioOptional = usuarioRepository.findById(id);
        if (usuarioOptional.isPresent()){
            usuario.setId(id);
            usuario.setDataCadastro(usuarioOptional.get().getDataCadastro());
            Usuario usuarioObjeto = usuarioRepository.save(usuario);

            UsuarioRespostaDTO usuarioRespostaDTO = new UsuarioRespostaDTO();
            usuarioRespostaDTO.converterUsuarioRespostaDTO(usuarioObjeto);
            return usuarioRespostaDTO;
        } else {
            throw new RuntimeException("O usuário " + id + " não foi encontrado.");
        }
    }

    public void deletar(int id) {
        if (usuarioRepository.existsById(id)) {
            usuarioRepository.deleteById(id);
        } else {
            throw new RuntimeException("O usuário " + id + " não foi encontrado.");
        }
    }
}
