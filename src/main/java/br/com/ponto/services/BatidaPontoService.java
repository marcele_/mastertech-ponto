package br.com.ponto.services;

import br.com.ponto.enums.TipoBatidaEnum;
import br.com.ponto.models.BatidaPonto;
import br.com.ponto.models.Usuario;
import br.com.ponto.models.dtos.BatidaPontoDTO;
import br.com.ponto.models.dtos.EspelhoPontoRespostaDTO;
import br.com.ponto.repositories.BatidaPontoRepository;
import br.com.ponto.repositories.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class BatidaPontoService {

    @Autowired
    private BatidaPontoRepository batidaPontoRepository;

    @Autowired
    private UsuarioRepository usuarioRepository;

    public BatidaPonto salvar(BatidaPonto batidaPonto){
        Optional<Usuario> optionalUsuario = usuarioRepository.findById(batidaPonto.getUsuario().getId());
        if (optionalUsuario.isPresent()) {

            String tipoUltimoRegistroPonto = batidaPontoRepository.buscarUltimoResgistroPontoDia(batidaPonto.getUsuario().getId());

            // 0 = ENTRADA
            // 1 = SAÍDA
            if ((tipoUltimoRegistroPonto == null) || (tipoUltimoRegistroPonto.equals("1"))){
                if (batidaPonto.getTipoBatida().name().equals("SAIDA")) {
                    throw new RuntimeException("A última marcação é do tipo SAIDA. " +
                            "A próxima marcação deverá ser do tipo ENTRADA.");
                } else {
                    batidaPonto.setTipoBatida(TipoBatidaEnum.ENTRADA);
                }
            } else {
                if (batidaPonto.getTipoBatida().name().equals("ENTRADA")) {
                    throw new RuntimeException("A última marcação é do tipo ENTRADA. " +
                            "A próxima marcação deverá ser do tipo SAIDA.");
                } else {
                    batidaPonto.setTipoBatida(TipoBatidaEnum.SAIDA);
                }
            }

            batidaPonto.setUsuario(optionalUsuario.get());
            batidaPonto.setDataHoraBatida(LocalDateTime.now());

            BatidaPonto batidaPontoSalvo = batidaPontoRepository.save(batidaPonto);
            BatidaPontoDTO batidaPontoDTO = new BatidaPontoDTO();
            batidaPontoDTO.converterBatidaPontoDTO(batidaPontoSalvo);
            return batidaPontoSalvo;
        }
        else{
            throw new RuntimeException("O usuário " + batidaPonto.getUsuario().getId() + " não existe.");
        }

    }

    public Iterable<BatidaPonto> consultar(){
        return batidaPontoRepository.findAll();
    }

    public EspelhoPontoRespostaDTO buscarPorIdUsuario(int id){
        //BatidaPonto batidaPonto = new BatidaPonto();
        Optional<Usuario> optionalUsuario = usuarioRepository.findById(id);
        if (optionalUsuario.isPresent()) {
            ArrayList<BatidaPonto> listaBatidaPontosDia = batidaPontoRepository.buscarTodosRegistrosDia(id);

            if(!listaBatidaPontosDia.isEmpty()){

                ArrayList<BatidaPontoDTO> listaBatidaPontosRespostaDTO = new ArrayList<>();

                for (BatidaPonto batidaPontoObjeto: listaBatidaPontosDia) {
                    BatidaPontoDTO batidaPontoDTO = new BatidaPontoDTO();
                    batidaPontoDTO.converterBatidaPontoDTO(batidaPontoObjeto);

                    listaBatidaPontosRespostaDTO.add(batidaPontoDTO);
                }

                EspelhoPontoRespostaDTO espelhoPontoRespostaDTO = new EspelhoPontoRespostaDTO();
                //espelhoPontoRespostaDTO.setTotalHorasTrabalhadas(LocalTime.now());
                espelhoPontoRespostaDTO.setTotalHorasTrabalhadas(calculaHorasTrabalhadasRegistrosDia(listaBatidaPontosRespostaDTO));
                espelhoPontoRespostaDTO.setBatidaPontoDTOS(listaBatidaPontosRespostaDTO);

                return espelhoPontoRespostaDTO;
            }else{
                throw new RuntimeException("O usuário " + id + " não tem nenhum registro de ponto.");
            }
        }else {
            throw new RuntimeException("O usuário " + id + " não existe.");
        }
    }

    public String calculaHorasTrabalhadasRegistrosDia(List<BatidaPontoDTO> listaBatidaPontosDTO){
        Date dataAtual = new Date();
        long miliSegundosEntrada = 0;
        long miliSegundosSaida = 0;

        for(BatidaPontoDTO batidaPontoDTO: listaBatidaPontosDTO){
            if(batidaPontoDTO.getTipoBatida().name().equals("ENTRADA")){
                miliSegundosEntrada += batidaPontoDTO.getDataHoraBatida().getHour();
            } else {
                miliSegundosSaida += batidaPontoDTO.getDataHoraBatida().getHour();
            }
        }

        //Marcação ainda esta em aberto!!
        if(listaBatidaPontosDTO.size()%2 != 0){
            miliSegundosSaida += dataAtual.getTime();
        }

        long miliSegundosResultado = (miliSegundosSaida - miliSegundosEntrada);
        int sec = (int) (miliSegundosResultado / 1000) % 60 ;
        int min = (int) ((miliSegundosResultado / (1000*60)) % 60);
        int horas   = (int) ((miliSegundosResultado / (1000*60*60)) % 24);

        String totalHorasTrabalhadasRegistrosDia = horas + ":"+min+ ":" +sec;

        return totalHorasTrabalhadasRegistrosDia;
    }

}
