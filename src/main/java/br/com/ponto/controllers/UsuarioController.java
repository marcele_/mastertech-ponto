package br.com.ponto.controllers;

import br.com.ponto.models.Usuario;
import br.com.ponto.models.dtos.UsuarioRespostaDTO;
import br.com.ponto.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/usuarios")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<UsuarioRespostaDTO> criar(@RequestBody @Valid Usuario usuario){
        try{
            UsuarioRespostaDTO usuarioRespostaDTO = usuarioService.salvar(usuario);
            return ResponseEntity.status(201).body(usuarioRespostaDTO);
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @PutMapping("/{id}")
    public UsuarioRespostaDTO alterar(@Valid @PathVariable(name = "id") int id, @RequestBody Usuario usuario){
        try {
            UsuarioRespostaDTO usuarioRespostaDTO = usuarioService.editar(id, usuario);
            return usuarioRespostaDTO;
        } catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }

    }

    @GetMapping("/{id}")
    public UsuarioRespostaDTO consultarPorId(@Valid @PathVariable(name = "id") int id){
        try {
            return usuarioService.buscarPorId(id);
        } catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @GetMapping
    public Iterable<UsuarioRespostaDTO> listar(){
        try{
            return usuarioService.buscar();
        }catch (Exception exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void excluir(@Valid @PathVariable(name = "id") int id){
        try{
            usuarioService.deletar(id);
        }catch (Exception exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

}
