package br.com.ponto.controllers;

import br.com.ponto.models.BatidaPonto;
import br.com.ponto.models.dtos.EspelhoPontoRespostaDTO;
import br.com.ponto.services.BatidaPontoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.ArrayList;

@RestController
@RequestMapping("/pontos")
public class BatidaPontoController {

    @Autowired
    private BatidaPontoService batidaPontoService;

    @PostMapping("/registrar")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<BatidaPonto> cadastrar(@Valid @RequestBody BatidaPonto batidaPonto){
        try{
            BatidaPonto batidaPontoObjeto = batidaPontoService.salvar(batidaPonto);
            return ResponseEntity.status(201).body(batidaPontoObjeto);
        }catch (Exception exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @GetMapping
    public Iterable<BatidaPonto> listar(){
        try{
            return batidaPontoService.consultar();
        }catch (Exception exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @GetMapping("/usuario/{id}")
    public EspelhoPontoRespostaDTO consultarPorId(@Valid @PathVariable(name = "id") int id){
        try {
            return batidaPontoService.buscarPorIdUsuario(id);
        } catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

}
