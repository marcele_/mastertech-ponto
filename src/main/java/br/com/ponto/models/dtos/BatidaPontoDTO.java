package br.com.ponto.models.dtos;

import br.com.ponto.enums.TipoBatidaEnum;
import br.com.ponto.models.BatidaPonto;

import java.time.LocalDateTime;

public class BatidaPontoDTO {

    private int idBatidaPonto;
    private LocalDateTime dataHoraBatida;
    private TipoBatidaEnum tipoBatida;

    public BatidaPontoDTO() {
    }

    public void converterBatidaPontoDTO(BatidaPonto batidaPonto) {
        setIdBatidaPonto(batidaPonto.getId());
        setTipoBatida(batidaPonto.getTipoBatida());
        setDataHoraBatida(batidaPonto.getDataHoraBatida());
    }

    public int getIdBatidaPonto() {
        return idBatidaPonto;
    }

    public void setIdBatidaPonto(int idBatidaPonto) {
        this.idBatidaPonto = idBatidaPonto;
    }

    public LocalDateTime getDataHoraBatida() {
        return dataHoraBatida;
    }

    public void setDataHoraBatida(LocalDateTime dataHoraBatida) {
        this.dataHoraBatida = dataHoraBatida;
    }

    public TipoBatidaEnum getTipoBatida() {
        return tipoBatida;
    }

    public void setTipoBatida(TipoBatidaEnum tipoBatida) {
        this.tipoBatida = tipoBatida;
    }
}
