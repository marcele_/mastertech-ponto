package br.com.ponto.models.dtos;

import br.com.ponto.models.Usuario;

import java.time.LocalDate;

public class UsuarioRespostaDTO {

    private int id;
    private String nomeCompleto;
    private String cpf;
    private String email;
    private LocalDate dataCadastro;

    public UsuarioRespostaDTO() {
    }

    public void converterUsuarioRespostaDTO(Usuario usuario) {
        setId(usuario.getId());
        setNomeCompleto(usuario.getNomeCompleto());
        setCpf(usuario.getCpf());
        setEmail(usuario.getEmail());
        setDataCadastro(usuario.getDataCadastro());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNomeCompleto() {
        return nomeCompleto;
    }

    public void setNomeCompleto(String nomeCompleto) {
        this.nomeCompleto = nomeCompleto;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(LocalDate dataCadastro) {
        this.dataCadastro = dataCadastro;
    }
}
