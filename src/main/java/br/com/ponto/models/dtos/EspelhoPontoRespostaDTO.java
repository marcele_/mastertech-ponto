package br.com.ponto.models.dtos;

import java.time.LocalTime;
import java.util.List;

public class EspelhoPontoRespostaDTO {

    private String totalHorasTrabalhadas;
    private List<BatidaPontoDTO> batidaPontoDTOS;

    public EspelhoPontoRespostaDTO() {
    }

    public String getTotalHorasTrabalhadas() {
        return totalHorasTrabalhadas;
    }

    public void setTotalHorasTrabalhadas(String totalHorasTrabalhadas) {
        this.totalHorasTrabalhadas = totalHorasTrabalhadas;
    }

    public List<BatidaPontoDTO> getBatidaPontoDTOS() {
        return batidaPontoDTOS;
    }

    public void setBatidaPontoDTOS(List<BatidaPontoDTO> batidaPontoDTOS) {
        this.batidaPontoDTOS = batidaPontoDTOS;
    }
}
