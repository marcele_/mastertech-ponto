package br.com.ponto.models;

import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Entity
public class Usuario {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull(message = "O nome do usuário não pode ser null.")
    @NotBlank(message = "O nome do usuário não pode estar em branco.")
    @Size(min = 2, max = 400, message = "O nome do usuário deve ter no mínimo 2 caracteres. ")
    private String nomeCompleto;

    @CPF(message = "CPF inválido!")
    @NotNull(message = "O CPF não pode ser null.")
    private String cpf;

    @Email(message = "E-mail inválido!")
    @NotNull
    @Size(min = 5, max = 400, message = "O email do usuário deve ter no mínimo 5 caracteres.")
    private String email;

    private LocalDate dataCadastro;

    public Usuario() {
    }

    public Usuario(@NotNull(message = "O nome do usuário não pode ser null.") @NotBlank(message = "O nome do usuário não pode estar em branco.") @Size(min = 2, max = 400, message = "O nome do usuário deve ter no mínimo 2 caracteres. ") String nomeCompleto, @CPF(message = "CPF inválido!") @NotNull(message = "O CPF não pode ser null.") String cpf, @Email(message = "E-mail inválido!") @NotNull @Size(min = 5, max = 400, message = "O email do usuário deve ter no mínimo 5 caracteres.") String email) {
        this.nomeCompleto = nomeCompleto;
        this.cpf = cpf;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNomeCompleto() {
        return nomeCompleto;
    }

    public void setNomeCompleto(String nomeCompleto) {
        this.nomeCompleto = nomeCompleto;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(LocalDate dataCadastro) {
        this.dataCadastro = dataCadastro;
    }
}
