package br.com.ponto.models;

import br.com.ponto.enums.TipoBatidaEnum;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
public class BatidaPonto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    private Usuario usuario;

    private LocalDateTime dataHoraBatida;

    private TipoBatidaEnum tipoBatida;

    public BatidaPonto() {
    }

    public BatidaPonto(@NotNull(message = "O usuário não pode ser null.") @NotBlank(message = "O usuário não pode estar em branco.") Usuario usuario, @NotNull(message = "Valor Entrada/Saída obrigatório.") @NotBlank(message = "Valor Entrada/Saída obrigatório.") TipoBatidaEnum tipoBatidaEnum) {
        this.usuario = usuario;
        this.tipoBatida = tipoBatidaEnum;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public LocalDateTime getDataHoraBatida() {
        return dataHoraBatida;
    }

    public void setDataHoraBatida(LocalDateTime dataHoraBatida) {
        this.dataHoraBatida = dataHoraBatida;
    }

    public TipoBatidaEnum getTipoBatida() {
        return tipoBatida;
    }

    public void setTipoBatida(TipoBatidaEnum tipoBatidaEnum) {
        this.tipoBatida = tipoBatidaEnum;
    }
}
