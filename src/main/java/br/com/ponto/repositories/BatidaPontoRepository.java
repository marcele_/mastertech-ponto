package br.com.ponto.repositories;

import br.com.ponto.models.BatidaPonto;
import br.com.ponto.models.Usuario;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public interface BatidaPontoRepository extends CrudRepository<BatidaPonto, Integer> {

    ArrayList<BatidaPonto> findByUsuario(Optional<Usuario> usuario);

    //SELECT * FROM batida_ponto WHERE usuario_id = "1" AND DATE(data_hora_batida) = CURDATE();
    @Query(value = "SELECT * FROM batida_ponto " +
            "WHERE usuario_id = :id_usuario " +
            "AND DATE(data_hora_batida) = CURDATE()", nativeQuery = true)
    ArrayList<BatidaPonto> buscarTodosRegistrosDia(@Param("id_usuario") int usuario_id);

    //"SELECT tipo_batida FROM batida_ponto WHERE usuario_id = 1 AND data_hora_batida = (SELECT MAX(data_hora_batida) FROM batida_ponto WHERE usuario_id = 1 AND DATE(data_hora_batida) = CURDATE());"
    @Query(value = "SELECT tipo_batida FROM batida_ponto " +
            "WHERE usuario_id = :id_usuario " +
            "AND data_hora_batida = (" +
                "SELECT MAX(data_hora_batida) " +
                    "FROM batida_ponto " +
                    "WHERE usuario_id = :id_usuario " +
                    "AND DATE(data_hora_batida) = CURDATE())", nativeQuery = true)
    String buscarUltimoResgistroPontoDia(@Param("id_usuario") int id_usuario);
}
